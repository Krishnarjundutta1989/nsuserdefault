//
//  UserNameViewController.m
//  NSUserDefault
//
//  Created by click labs 115 on 11/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "UserNameViewController.h"

@interface UserNameViewController ()
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;

@end

@implementation UserNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *UserDataUsername = [[NSUserDefaults standardUserDefaults]objectForKey:@"loginUserName"];
    
    // Dispose of any resources that can be recreated.
    
    if ([UserDataUsername isEqualToString: @""]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Valid UserName" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else {
        _lblUserName.text =[NSString stringWithFormat:@"UserName :  %@", UserDataUsername] ;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"UserName" message:[NSString stringWithFormat:@"%@", _lblUserName.text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];


    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

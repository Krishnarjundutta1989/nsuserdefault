//
//  ViewController.m
//  NSUserDefault
//
//  Created by click labs 115 on 11/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSString *userName ;
    NSString *password ;
}
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtPassword.secureTextEntry = YES;
  
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)SaveData:(id)sender {
    userName =[NSString stringWithFormat:@"%@",  _txtUserName.text];
    password = [NSString stringWithFormat:@"%@", _txtPassword.text];
    [[NSUserDefaults standardUserDefaults]setObject:userName forKey:@"loginUserName"];
    [[NSUserDefaults standardUserDefaults]setObject:password forKey:@"loginPassword"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Login Successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    _txtUserName.text = @"";
    _txtPassword.text = @"";

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

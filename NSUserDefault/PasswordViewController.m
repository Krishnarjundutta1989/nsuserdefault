//
//  PasswordViewController.m
//  NSUserDefault
//
//  Created by click labs 115 on 11/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()
@property (strong, nonatomic) IBOutlet UILabel *lblPassword;

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *UserDataPassword = [[NSUserDefaults standardUserDefaults]objectForKey:@"loginPassword"];
    
    // Dispose of any resources that can be recreated.
    
    if ([UserDataPassword isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Valid Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

        
    }
    else {
        _lblPassword.text =[NSString stringWithFormat:@"Password :  %@", UserDataPassword] ;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password" message:[NSString stringWithFormat:@"%@", _lblPassword.text] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];


           }

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

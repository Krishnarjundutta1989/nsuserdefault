//
//  AppDelegate.h
//  NSUserDefault
//
//  Created by click labs 115 on 11/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


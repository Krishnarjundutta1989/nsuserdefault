//
//  main.m
//  NSUserDefault
//
//  Created by click labs 115 on 11/17/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
